import pandas as pd
import numpy as np
import matplotlib.pyplot as plot

df_main = pd.read_csv("Indicators.csv")
df_gdp = df_main.loc[(df_main['CountryName'] == 'Brazil') & (df_main['IndicatorCode'] == 'NY.GDP.MKTP.KD')]
# print(df_gdb)

plot.scatter(df_gdp['Year'], df_gdp['Value'])
plot.show()
